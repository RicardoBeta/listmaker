package com.awd.listmaker

import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class ListViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
    val listItemid = itemView.findViewById<TextView>(R.id.Item_id)
    val listItemTitle = itemView.findViewById<TextView>(R.id.item_title)
}