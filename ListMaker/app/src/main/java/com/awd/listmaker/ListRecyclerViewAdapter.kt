package com.awd.listmaker

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView

class ListRecyclerViewAdapter: RecyclerView.Adapter<ListViewHolder>() {
    val mainList = arrayOf("shopping List", "Homework","Chores")

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ListViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.view_holder,parent,false)
        return ListViewHolder(view)
    }

    override fun getItemCount(): Int {
        return mainList.size
    }

    override fun onBindViewHolder(holder: ListViewHolder, position: Int) {
        holder.listItemid.text = position.toString()
        holder.listItemTitle.text = mainList[position]
    }

}